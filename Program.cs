using Google.Ads.GoogleAds.Lib;
using Google.Ads.GoogleAds.V6.Errors;
using Google.Ads.GoogleAds.V6.Services;
using Google.Api.Gax;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using static Google.Ads.GoogleAds.V6.Services.GoogleAdsServiceClient;

namespace Google.Ads.GoogleAds.Examples.V6
{
    /// <summary>
    /// This code example gets all campaigns. To add campaigns, run AddCampaigns.cs.
    /// </summary>
    public class GetCampaigns : ExampleBase
    {
        /// <summary>
        /// Main method, to run this code example as a standalone application.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void Main(string[] args)
        {
            //Environment.SetEnvironmentVariable("GRPC_DNS_RESOLVER", "native"); // Necessary in nuget version 1.1.0 to work around bug in code 

            //SslCredentials secureCredentials = new SslCredentials(File.ReadAllText("certificate.pem"));
            //var channel = new Channel("localhost", 5001, secureCredentials);

            //Channel channel = new Channel("127.0.0.1:5001", ChannelCredentials.Insecure);
            //THIS IS YOUR CLIENT'S CERTIFICATE AND IT'S KEY

            //         List<ChannelOption> channelOptions = new List<ChannelOption>()
            //{
            //    new ChannelOption("grpc.ssl_target_name_override", " "),
            //};

            //         SslCredentials secureCredentials = new SslCredentials(File.ReadAllText("certificate.pem"));
            //         var channel = new Channel("localhost", 5001, secureCredentials, channelOptions);


            GetCampaigns codeExample = new GetCampaigns();

            Console.WriteLine(codeExample.Description);

            // The Google Ads customer ID for which the call is made.
            long customerId = long.Parse("8015371150");

            codeExample.Run(new GoogleAdsClient(), customerId);
        }

        /// <summary>
        /// Returns a description about the code example.
        /// </summary>
        public override string Description =>
            "This code example gets all campaigns. To add campaigns, run AddCampaigns.cs.";

        /// <summary>
        /// Runs the code example.
        /// </summary>
        /// <param name="client">The Google Ads client.</param>
        /// <param name="customerId">The Google Ads customer ID for which the call is made.</param>
        public void Run(GoogleAdsClient client, long customerId)
        {
            // Get the GoogleAdsService.             
            GoogleAdsServiceClient googleAdsService = client.GetService(
                Services.V6.GoogleAdsService);            
            
            // Create a query that will retrieve all campaigns.
            string query = @"SELECT
                            campaign.id,
                            campaign.name,
                            campaign.network_settings.target_content_network
                        FROM campaign
                        ORDER BY campaign.id";

            try
            {
                // Issue a search request.
                googleAdsService.SearchStream(customerId.ToString(), query,
                    delegate (SearchGoogleAdsStreamResponse resp)
                    {
                        foreach (GoogleAdsRow googleAdsRow in resp.Results)
                        {
                            Console.WriteLine("Campaign with ID {0} and name '{1}' was found.",
                                googleAdsRow.Campaign.Id, googleAdsRow.Campaign.Name);
                        }
                    }
                );
            }
            catch (GoogleAdsException e)
            {
                Console.WriteLine("Failure:");
                Console.WriteLine($"Message: {e.Message}");
                Console.WriteLine($"Failure: {e.Failure}");
                Console.WriteLine($"Request ID: {e.RequestId}");
                Console.ReadLine();
                throw;
                
            }
        }
    }
}
